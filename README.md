# Bootleg

Bootleg is an open-source "monster-hacking" RPG project, inspired by a famous creature-capture RPG serie that I don't need to name ;-)

But it won't aim to be just a clone, but more of a new experience inspired by this type of game. It'll add a twist to this now-classic type of game by using the whole world of glitches, creepypastas and rumors that spawned from these. It won't be an horror game though, but mostly a game that take inspiration from this kind of things to create a new experience.

The game will use Master System styled graphics, in order to spice a bit the 8bit graphics experience.
