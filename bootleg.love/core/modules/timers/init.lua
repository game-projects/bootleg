-- timers :: a basic implementation of a timer system.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd = (...):gsub('%.init$', '') .. "."

local TimersManager = Object:extend()
local Timer = require(cwd .. "libs.timer")
local Tween = require(cwd .. "libs.tween")


function TimersManager:new(subject)
  self.subject  = subject
  self.time    = 0

  self.timers   = {}
  self.switches = {}
  self.tweens   = {}
end

-- UPDATE
-- Update every timers

function TimersManager:update(dt)
  self.time = self.time + dt

  for k, timer in pairs(self.timers) do
    timer:update(dt)
  end

  self:updateSwitches(dt)
  self:updateTweens(dt)
end

-- TIMER FUNCTIONS
-- Handle the timers

function TimersManager:newTimer(duration, name)
  self.timers[name] = Timer(self, duration, name)
end

function TimersManager:timerResponse(timername)
  if self.subject.timerResponse == nil then
    core.debug:warning("TimersManager", "the subject have no timerResponse function")
    return 0
  end
  self.subject:timerResponse(timername)
end

-- SWITCH FUNCTIONS
-- Handle switches

function TimersManager:newSwitch(start, bools)
  local newSwitch = {}
  -- we add the data into a switch wrapper
  newSwitch.bools = bools
  newSwitch.start = self.time + start -- /!\ START IS RELATIVE TO CURRENT TIME
  newSwitch.clear = newSwitch.start + 1

  table.insert(self.switches, newSwitch)
end

function TimersManager:updateSwitches(dt)
  for i, switch in ipairs(self.switches) do
    if (self.time > switch.start) then
      -- We test each boolean in the switch
      for i, bool in ipairs(switch.bools) do
        -- if it's nil, we set it to true
        if self.subject[bool] == nil then
          self.subject[bool] = true
        else
          -- if it's not nil, we reverse the boolean
          self.subject[bool] = (self.subject[bool] == false)
        end
      end
      table.remove(self.switches, i)
    end
  end
end

-- TWEENING FUNCTIONS
-- Handle tween support via tween.lua

function TimersManager:newTween(start, duration, target, easing)
  local newTween = {}
  -- we add the data into a tween wrapper
  newTween.tween = Tween.new(duration, self.subject, target, easing)
  newTween.start = self.time + start -- /!\ START IS RELATIVE TO CURRENT TIME
  newTween.clear = newTween.start + duration

  table.insert(self.tweens, newTween)
end

function TimersManager:updateTweens(dt)
  for i, tweenWrapper in ipairs(self.tweens) do
    if (self.time > tweenWrapper.start) then
      tweenWrapper.tween:update(dt)
    end
  end

  self:clearEndedTweens()
end

function TimersManager:clearEndedTweens()
  for i, tweenWrapper in ipairs(self.tweens) do
    if (self.time > tweenWrapper.clear) then
      table.remove(self.tweens, i)
    end
  end
end

return TimersManager
