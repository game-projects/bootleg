-- assets/sprite :: the assets object, which is basically a tileset animated by
-- an animator object. An animator object is always tied to a sprite, but a sprite
-- can use different animator in order to make several animator share the same sprite

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Sprite    = Object:extend()
local cwd  = (...):gsub('%.sprites$', '') .. "."

local Animator  = require(cwd .. "animator")
local Tileset   = require(cwd .. "tileset")

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Sprite:new(filepath)
  self.tileset  = Tileset(filepath)
  self.data     = require(filepath)

  self.animator = Animator(self)

  self.customSpeed = 0

  self:changeToDefaultAnimation(true)
end

function Sprite:update(dt)
  self.animator:update(dt)
end

function Sprite:clone()
  return Animator(self)
end

function Sprite:setCustomSpeed(customSpeed)
  self.animator:setCustomSpeed(customSpeed)
end

function Sprite:changeToDefaultAnimation(restart)
  self.animator:changeToDefaultAnimation(restart)
end

function Sprite:changeAnimation(name, restart)
  self.animator:changeAnimation(name, restart)
end

-- INFO FUNCTIONS
-- get information with these functions

function Sprite:getCurrentAnimation()
  return self.animator:getCurrentAnimation()
end

function Sprite:animationExist(name)
  return self.animator:animationExist(name)
end

function Sprite:getDimensions()
  return self.tileset:getDimensions()
end

function Sprite:getFrame()
  return self.animator:getFrame()
end

function Sprite:getAnimationDuration(animation)
  return self.animator:getAnimationDuration(animation)
end

function Sprite:getRelativeFrame()
  return self.animator:getRelativeFrame()
end

-- DRAW FUNCTIONS
-- Draw sprites using these functions

function Sprite:drawAnimation(x, y, r, sx, sy, ox, oy, kx, ky)
  self.animator:draw(x, y, r, sx, sy, ox, oy, kx, ky)
end

function Sprite:drawAnimationMask(x, y, r, sx, sy, ox, oy, kx, ky)
  self.animator:drawMask(x, y, r, sx, sy, ox, oy, kx, ky)
end

function Sprite:drawFrame(frame, x, y, r, sx, sy, ox, oy, kx, ky)
  self.tileset:drawTile(frame, x, y, r, sx, sy, ox, oy, kx, ky)
end

function Sprite:drawFrameMask(frame, x, y, r, sx, sy, ox, oy, kx, ky)
  self.tileset:drawTileMask(frame, x, y, r, sx, sy, ox, oy, kx, ky)
end

function Sprite:drawPart(x, y, w, h, r, sx, sy, ox, oy, kx, ky)
  local w = math.floor(w)
  local h = math.floor(h)

  if w >= 0 and h <= 0 then
    return 0
  end

  love.graphics.setScissor(x - ox, y - oy, w, h)
  self:drawAnimation(x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.setScissor( )
end

return Sprite
