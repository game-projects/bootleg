-- assets/autotile :: The autotile object : this is an object that draw tiles
-- automatically with borders in rectangles.
-- It works with a 3×3 tileset showing all borders.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.autotile$', '') .. "."
local Tileset = require(cwd .. "tileset")
local Autotile = Object:extend()

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Autotile:new(filepath)
  self.tileset = Tileset(filepath)

  self.data     = require(filepath .. ".lua")
  self.metadata = self.data.metadata

  self.tilesize = self.metadata.width
end

-- DRAW FUNCTIONS
-- Draw tileset using these functions

function Autotile:drawtile(i, j, x, y, r, sx, sy, ox, oy, kx, ky)
  local i = i or 1
  local j = j or 1
  local tilesize = self.tilesize / 2
  i = (i - 1) * 2 + 1
  j = (j - 1) * 2 + 1
  self.tileset:drawTile_Grid(i    , j    , x           , y           , r, sx, sy, ox, oy, kx, ky)
  self.tileset:drawTile_Grid(i + 1, j    , x + tilesize, y           , r, sx, sy, ox, oy, kx, ky)
  self.tileset:drawTile_Grid(i    , j + 1, x           , y + tilesize, r, sx, sy, ox, oy, kx, ky)
  self.tileset:drawTile_Grid(i + 1, j + 1, x + tilesize, y + tilesize, r, sx, sy, ox, oy, kx, ky)
end

function Autotile:draw(x, y, w, h)
  local w = w or self.tilesize
  local h = h or self.tilesize
  w = math.max(math.floor(w / self.tilesize), 1)
  h = math.max(math.floor(h / self.tilesize), 1)
  local halfsize = self.tilesize / 2
  local tilesize = self.tilesize
  if (w == 1) then
    self.tileset:drawtile_Grid(1, 1, x                     , y)
    self.tileset:drawtile_Grid(1, 6, x                     , y + (h*2 - 1) * halfsize)
    self.tileset:drawtile_Grid(6, 1, x + (w*2 - 1) * halfsize, y)
    self.tileset:drawtile_Grid(6, 6, x + (w*2 - 1) * halfsize, y + (h*2 - 1) * halfsize)
    if (h > 1) then
      h = h - 1
      for i = 1, h do
        self.tileset:drawtile_Grid(1, 3, x, y + (i * tilesize) - halfsize)
        self.tileset:drawtile_Grid(6, 3, x + halfsize, y + (i * tilesize) - halfsize)

        self.tileset:drawtile_Grid(1, 4, x           , y + (i * tilesize))
        self.tileset:drawtile_Grid(6, 4, x + halfsize, y + (i * tilesize))
      end
    end
    -- draw just one stuff
  else
    if (h == 1) then
      self.tileset:drawtile_Grid(1, 1, x                     , y)
      self.tileset:drawtile_Grid(1, 6, x                     , y + (h*2 - 1) * halfsize)
      self.tileset:drawtile_Grid(6, 1, x + (w*2 - 1) * halfsize, y)
      self.tileset:drawtile_Grid(6, 6, x + (w*2 - 1) * halfsize, y + (h*2 - 1) * halfsize)
      w = w - 1
      for i = 1, w do
        self.tileset:drawtile_Grid(3, 1, x + (i * tilesize) - halfsize, y)
        self.tileset:drawtile_Grid(3, 6, x + (i * tilesize) - halfsize, y + halfsize)

        self.tileset:drawtile_Grid(4, 1, x + (i * tilesize)          , y )
        self.tileset:drawtile_Grid(4, 6, x + (i * tilesize), y +halfsize)
      end
    else
      self:drawtile(1, 1, x                     , y)
      self:drawtile(1, 3, x                     , y + (h - 1) * tilesize)
      self:drawtile(3, 1, x + (w - 1) * tilesize, y)
      self:drawtile(3, 3, x + (w - 1) * tilesize, y + (h - 1) * tilesize)
      w = w - 2
      h = h - 2
      for i=1, w do
        self:drawtile(2, 1, i * tilesize, y)
        self:drawtile(2, 3, i * tilesize, y + (h + 1) * tilesize)
        for j=1, h do
          self:drawtile(2, 2, i * tilesize, j * tilesize)
        end
      end

      for i=1, h do
        self:drawtile(1, 2, x                     , i * tilesize)
        self:drawtile(3, 2, x + (w + 1) * tilesize, i * tilesize)
      end
    end
  end
end

return Autotile
