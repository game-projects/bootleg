local ParentMap = Object:extend()

-- INIT FUNCTION
-- Initialize the map

function ParentMap:new(world, r, g, b)
  self.world = world

  local r = r or 128
  local g = g or 128
  local b = b or 128
  self.backgroundColor = {r, g, b}

  self:setPadding()
  self:register()
end

function ParentMap:register()
  self.world.map = self
end

function ParentMap:setPadding(x1, y1, x2, y2)
  local padding = {}
  padding.x1 = x1 or 0
  padding.y1 = y1 or padding.x1
  padding.x2 = x2 or padding.x1
  padding.y2 = y2 or padding.y1

  self.padding = padding
end

-- UPDATE FUNCTION
-- Update or modify the map

function ParentMap:resize(w, h)
  -- Empty Placeholder function
end

function ParentMap:update(dt)
  -- Empty Placeholder function
end

function ParentMap:loadObjects()
  self:loadCollisions()
  self:loadPlayers()
  self:loadActors()
end

function ParentMap:loadCollisions()
  -- Empty Placeholder function
end

function ParentMap:loadPlayers()
  -- Empty Placeholder function
end

function ParentMap:loadActors()
  -- Empty Placeholder function
end

function ParentMap:setBackgroundColor(r, g, b)
  local r = r or 128
  local g = g or 128
  local b = b or 128
  self.backgroundColor = {r, g, b}
end

function ParentMap:setBackgroundColorFromTable(backgroundColor)
  self.backgroundColor = backgroundColor or {128, 128, 128}
end

function ParentMap:getBackgroundColor()
  return self.backgroundColor[1]/256, self.backgroundColor[2]/256, self.backgroundColor[3]/256
end

function ParentMap:getPadding()
  return self.padding.x1, self.padding.y1, self.padding.x2, self.padding.y2
end

function ParentMap:getDimensions()
  return core.screen:getDimensions()
end

function ParentMap:getBox()
  local x1, y1, x2, y2 = self:getPadding()
  local w, h = self:getDimensions()
  return -x1, -y1, w+x2, h+y2
end

function ParentMap:drawBackgroundColor()
  local r, g, b = self.backgroundColor[1], self.backgroundColor[2], self.backgroundColor[3]
  love.graphics.setColor(r/256, g/256, b/256)
  love.graphics.rectangle("fill", 0, 0, 480, 272)
  utils.graphics.resetColor()
end

return ParentMap
