-- BaseActor.lua :: the global implementation of an actor. Basically, it abstract
-- everything that isn't only 2D or 3D related to the actor system.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd   = (...):gsub('%.baseactor$', '') .. "."
local BaseActor = Object:extend()

local Timer   = require(cwd .. "utils.timer")

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function BaseActor:new(world, type, x, y, z, w, h, d, isSolid)
  self.type     = type or ""
  self.isSolid  = isSolid or false
  self.depth    = 0

  self:setManagers(world)
  self:initKeys()
  self:initTimers()
  self:setSprite()
  self:initPhysics(x, y, z, w, h, d)

  self:setDebugColor(1, 1, 1)
  self:register()
end

function BaseActor:setManagers(world)
  self.world  = world
  self.scene  = world.scene
  self.obj    = world.obj
  self.assets = self.scene.assets
end

function BaseActor:setDebugColor(r,g,b)
  self.debug = {}
  self.debug.r = r
  self.debug.g = g
  self.debug.b = b
end

function BaseActor:register()
  self.world:registerActor(self)
  self.isDestroyed = false
end

function BaseActor:destroy()
  self.world:removeActor(self)
  self.isDestroyed = true
end

-- PHYSICS FUNCTIONS
-- Raw implementation of everything common in physics

function BaseActor:initPhysics(x, y, z, w, h, d)
  self:setCoordinate(x, y, z)

  self.w      = w or 0
  self.h      = h or 0
  self.d      = d or 0

  self.xsp    = 0
  self.ysp    = 0
  self.zsp    = 0

  self.xfrc   = 0
  self.yfrc   = 0
  self.zfrc   = 0

  self:initGravity()

  self:setBounceFactor()
  self:setFilter()
end

function BaseActor:setCoordinate(x, y, z, w, h, d)
  self.x = x or self.x
  self.y = y or self.y
  self.z = z or self.z
end

function BaseActor:setBounceFactor(newBounceFactor)
  self.bounceFactor = newBounceFactor or 0
end

function BaseActor:setFilter()
  -- Init the bump filter
  self.filter = function(item, other)
    if (other.owner == self) then
      -- ignore every collision with our own hitboxes
      return nil
    elseif (other.isSolid) then
      return "slide"
    else
      return "cross"
    end
  end
end

function BaseActor:getFuturePosition(dt)
  local dx, dy
  dx = self.x + self.xsp * dt
  dy = self.y + self.ysp * dt
  dz = self.z + self.zsp * dt

  return dx, dy, dz
end

function BaseActor:applyFriction(dt)
  self.xsp = utils.math.toZero(self.xsp, self.xfrc * dt)
  self.ysp = utils.math.toZero(self.ysp, self.yfrc * dt)
  self.zsp = utils.math.toZero(self.zsp, self.zfrc * dt)
end

function BaseActor:solveAllCollisions(cols)
  for i, col in ipairs(cols) do
    self:collisionResponse(col)
    if (col.type == "touch") or (col.type == "bounce") or (col.type == "slide") then
      self:changeSpeedToCollisionNormal(col.normal)
    end
  end
end

function BaseActor:collisionResponse(collision)
  -- here come the response to the collision
end

function BaseActor:changeSpeedToCollisionNormal(normal)
  -- Empty function in baseactor
end

-- COORDINATE/MOVEMENT FUNCTIONS
-- Handle coordinate

function BaseActor:getCenter()
  return (self.x + (self.w / 2)), (self.y + (self.h / 2)), (self.z + (self.d / 2))
end

function BaseActor:getViewCenter()
  return self:getCenter()
end

-- GRAVITY SYSTEM FUNCTIONS
-- All functions related to gravity

function BaseActor:initGravity()
  if (self.world.gravity.isDefault) then
    self.grav = self.world.gravity.grav
  else
    self.grav = 0
  end

  self.onGround = false
end

function BaseActor:setGravity(grav)
  -- It's basically now a function with two roles at once :
  -- - activate the gravity
  -- - use the gravity value the dev want

  self.grav = grav or self.world.gravity.grav
end

function BaseActor:applyGravity(dt)
  -- Empty function in baseactor
end

function BaseActor:checkGround()
  -- Empty function in baseactor
end

-- UPDATE FUNCTIONS
-- Theses functions are activated every steps

function BaseActor:updateStart(dt)

end

function BaseActor:update(dt)
  self:updateStart(dt)
  self:updateTimers(dt)
  self:autoMove(dt)
  self:updateSprite(dt)
  self:updateEnd(dt)
end

function BaseActor:updateEnd(dt)

end

function BaseActor:autoMove(dt)
  -- The base actor don't have coordinate
  -- so the autoMove is only usefull to its
  -- 2D and 3D childrens
end

-- INPUT FUNCTIONS
-- get input from the world object

function BaseActor:initKeys()
  self.keys = core.input.fakekeys
end

function BaseActor:getInput(keys)
  self.keys = keys or core.input.fakekeys
end

-- TIMER FUNCTIONS
-- Control the integrated timers of the actor

function BaseActor:initTimers()
  self.timers = core.modules.Timers(self)
end

function BaseActor:addTimer(name, t)
  core.debug:warning("actor", "function actor:addTimer is deprecated, prefer actor.timers:newTimer")
  self.timers:newTimer(t, name)
end

function BaseActor:updateTimers(dt)
  self.timers:update(dt)
end

function BaseActor:timerResponse(name)
  -- here come the timer responses
end

-- HITBOX FUNCTIONS
-- All functions to handle hitboxes

function BaseActor:initHitboxes()
  self:initMainHitbox()

  self.hitboxes = {}
  self.hitboxListFile = ""
  self.hitboxList = nil
end

function BaseActor:initMainHitbox()
  -- Empty function : don't load ANY real hitbox function into baseactor
end

function BaseActor:setHitboxFile(file)
  self.hitboxList = require(file)
  self.hitboxListFile = file
end

function BaseActor:getAutomaticHitboxLoading()
  return (self.hitboxList ~= nil)
end

function BaseActor:getHitboxFile()
  return self.hitboxListFile
end

function BaseActor:getHitboxList(animation, frame)
  if (animation == nil) or (self.hitboxList == nil) then
    return self.hitboxList
  else
    local list = self.hitboxList[animation]

    if (frame == nil) or (list == nil) then
      return list
    else
      return list[frame]
    end
  end
end

function BaseActor:updateHitboxes()
  if (self.hitboxList ~= nil) then
    self:purgeHitbox()
    local animation, frame
    animation = self:getCurrentAnimation()
    frame     = self:getRelativeFrame()
    local hitboxList = self:getHitboxList(animation, frame)

    if (hitboxList ~= nil) then
      for i,v in ipairs(hitboxList) do
        self:addHitboxFromFrameData(v, animation, frame, i)
      end
    end
  end
end

function BaseActor:checkHitboxesCollisions(filter)
  for k,v in pairs(self.hitboxes) do
    self:checkHitboxCollisions(k, filter)
  end
end

function BaseActor:hitboxResponse(name, type, collision)
  -- just a blank placeholder function
end

function BaseActor:removeHitbox(name)
  if (self.hitboxes[name] ~= nil) then
    self.hitboxes[name]:destroy()
    self.hitboxes[name] = nil
  end
end

function BaseActor:purgeHitbox()
  for k,v in pairs(self.hitboxes) do
    v:destroy()
  end
  self.hitboxes = {}
end

function BaseActor:drawHitboxes()
  for k,v in pairs(self.hitboxes) do
    v:draw()
  end
  self:drawMainHitbox()
end

function BaseActor:drawMainHitbox()
  if (self.mainHitbox ~= nil) then
    self.mainHitbox:draw()
  end
end

-- DRAW FUNCTIONS
-- Draw the actors.

function BaseActor:drawStart()

end

function BaseActor:draw()
  self:drawStart()
  self:drawEnd()
end

function BaseActor:drawEnd()

end

function BaseActor:drawHUD(id, height, width)

end

-- SPRITES FUNCTIONS
-- Handle the sprite of the actor

function BaseActor:setSprite(spritename, ox, oy)
  self.sprite = {}
  self.sprite.name    = spritename or nil
  self.sprite.ox      = ox or 0
  self.sprite.oy      = oy or 0
  self.sprite.sx      = 1
  self.sprite.sy      = 1
  self.sprite.exist   = (spritename ~= nil)
  self.sprite.clone   = nil
end

function BaseActor:cloneSprite()
  if self.sprite.name ~= nil then
    self.sprite.clone = self.assets.sprites[self.sprite.name]:clone()
    self.sprite.clone:setCallbackTarget(self)
  end
end

function BaseActor:changeAnimation(animation, restart)
  if (self.sprite.clone == nil) then
    self.assets.sprites[self.sprite.name]:changeAnimation(animation, restart)
  else
    self.sprite.clone:changeAnimation(animation, restart)
  end
end

function BaseActor:animationEnded(animation)
  -- Empty placeholder function
end

function BaseActor:setCustomSpeed(customSpeed)
  if (self.sprite.clone == nil) then
    self.assets.sprites[self.sprite.name]:setCustomSpeed(customSpeed)
  else
    self.sprite.clone:setCustomSpeed(customSpeed)
  end
end

function BaseActor:updateSprite(dt)
  if (self.sprite.clone ~= nil) then
    self.sprite.clone:update(dt)
  end
end

function BaseActor:setSpriteScallingX(sx)
  local sx = sx or 1

  self.sprite.sx = sx
end

function BaseActor:setSpriteScallingY(sy)
  local sy = sy or 1

  self.sprite.sy = sy
end

function BaseActor:getCurrentAnimation()
  if (self.sprite.clone == nil) then
    return self.assets.sprites[self.sprite.name]:getCurrentAnimation()
  else
    return self.sprite.clone:getCurrentAnimation()
  end
end


function BaseActor:getSpriteScalling()
  return self.sprite.sx, self.sprite.sy
end

function BaseActor:getFrame()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getFrame()
    else
      return self.assets.sprites[self.sprite.name]:getFrame()
    end
  end
end

function BaseActor:getRelativeFrame()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getRelativeFrame()
    else
      return self.assets.sprites[self.sprite.name]:getRelativeFrame()
    end
  end
end

function BaseActor:getAnimationDuration()
  if (self.sprite.name ~= nil) then
    if (self.sprite.clone ~= nil) then
      return self.sprite.clone:getAnimationDuration()
    else
      return self.assets.sprites[self.sprite.name]:getAnimationDuration()
    end
  end
end

function BaseActor:drawSprite(x, y, r, sx, sy, ox, oy, kx, ky)
  if (self.sprite.name ~= nil) then
    local x = x + self.sprite.ox
    local y = y + self.sprite.oy
    local sx = sx or self.sprite.sx
    local sy = sy or self.sprite.sy
    if (self.sprite.clone ~= nil) then
      self.sprite.clone:draw(x, y, r, sx, sy, ox, oy, kx, ky)
    else
      self.assets.sprites[self.sprite.name]:drawAnimation(x, y, r, sx, sy, ox, oy, kx, ky)
    end
  end
end

return BaseActor
