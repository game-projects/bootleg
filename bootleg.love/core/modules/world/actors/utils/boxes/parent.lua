-- box3D :: drawable box with shadow handling for fake3D actors

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Box3D = Object:extend()

function Box3D:new(owner, w, h, d, isVisible)
  self.owner    = owner
  self.world    = owner.world
  self.cameras  = self.world.cameras

  self.w = w
  self.h = h
  self.d = d

  self.haveLine = true

  self.shadowSources = {}
  self.needRedraw = false

  if (isVisible == nil) then
    self.isVisible  = true
  else
    self.isVisible  = isVisible
  end

  if (self.isVisible) then
    self:setTexture()
  end

  self.shadows = love.graphics.newCanvas(self.w, self.h)

  self:register()
end

function Box3D:register()
  self.owner.box = self
  self.world:registerTerrain(self.owner)
end

function Box3D:setSizeFromOwner()
  self:setSize(self.owner.w, self.owner.h, self.owner.d)
end

function Box3D:setSize()
  self.w = w
  self.h = h
  self.d = d

  self:regenerate()
end

function Box3D:setTexture()
  local canvas = love.graphics.newCanvas(self.w, self.h + self.d)
  love.graphics.setCanvas( canvas )
  utils.graphics.resetColor()

  self:drawTextureContent()

  love.graphics.setCanvas( )
  local imagedata = canvas:newImageData()
  self.texture = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()
end

function Box3D:drawTextureContent()
  self:drawTopTexture()
  self:drawBottomTexture()
end

function Box3D:drawTopTexture()
  utils.graphics.resetColor()
  love.graphics.rectangle("fill", 0, 0, self.w, self.h)
end

function Box3D:drawBottomTexture()
  love.graphics.setColor(0.9, 0.9, 0.9, 1)
  love.graphics.rectangle("fill", 0, self.h, self.w, self.d)
end

function Box3D:setShadowSource(actor, x, y)
  local foundShadow = false

  for i,v in ipairs(self.shadowSources) do
    if (v.actor == actor) then
      if (v.x ~= x) or (v.y ~= y) then
        v.x = x
        v.y = y
        self.needRedraw = true
      end
      foundShadow = true
    end
  end

  if (foundShadow == false) then
    local shadow = {}
    shadow.actor = actor
    shadow.x = x
    shadow.y = y
    self.needRedraw   = true

    table.insert(self.shadowSources, shadow)
  end
end

function Box3D:removeShadowSource(actor)
  for i,v in ipairs(self.shadowSources) do
    if (v.actor == actor) then
      table.remove(self.shadowSources, i)
      self.needRedraw = true
    end
  end
end

function Box3D:redrawShadowCanvas()
  if (self.needRedraw) then
    love.graphics.setCanvas( self.shadows )
    love.graphics.clear()
    for i,v in ipairs(self.shadowSources) do
      v.actor:drawShadow(v.x, v.y)
    end

    love.graphics.setCanvas( )

    self.needRedraw = false
  end
end

function Box3D:draw(x, y, z)
  if (self.isVisible) then
    love.graphics.setColor(0, 0, 0, 1)
    if (self.haveLine) then
      love.graphics.rectangle("line", x, (y-z) - (self.d), self.w, self.d + self.h)
    end
    utils.graphics.resetColor()
    love.graphics.draw(self.texture, x, (y-z) - (self.d))
  end

  if (self.shadows ~= nil) and (#self.shadowSources > 0) then
    love.graphics.draw(self.shadows, x, (y-z) - (self.d))
  end
end

return Box3D
