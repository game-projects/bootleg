-- textured.lua :: a textured box

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd   = (...):gsub('%.textured$', '') .. "."
local Box3D = require(cwd .. "parent")

local TexturedBox = Box3D:extend()

function TexturedBox:new(owner, w, h, d, topTexture, bottomTexture)
  local bottomTexture = bottomTexture or topTexture

  self.topTexture     = owner.assets.images[topTexture]
  self.bottomTexture  = owner.assets.images[bottomTexture]

  TexturedBox.super.new(self, owner, w, h, d)
  self.haveLine = false
end

function TexturedBox:drawTopTexture()
  local w, h = self.topTexture:getDimensions()
  local sx = self.w / w
  local sy = self.h / h
  self.topTexture:draw(0, 0, 0, sx, sy)
end

function TexturedBox:drawBottomTexture()
  local w, h = self.bottomTexture:getDimensions()
  local sx = self.w / w
  local sy = self.d / h
  self.bottomTexture:draw(0, self.h, 0, sx, sy)
end

return TexturedBox
