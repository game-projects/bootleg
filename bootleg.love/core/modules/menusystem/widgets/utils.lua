-- widgets/utils :: basic utility functions for the widgets

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local menuUtils = {}

function menuUtils.drawCursor(x, y, w, h)
  love.graphics.setColor(0,0,0)

  love.graphics.rectangle("fill", x, y, 4, 8)
  love.graphics.rectangle("fill", x, y, 8, 4)

  love.graphics.rectangle("fill", x + w, y, -4, 8)
  love.graphics.rectangle("fill", x + w, y, -8, 4)

  love.graphics.rectangle("fill", x, y + h, 4, -8)
  love.graphics.rectangle("fill", x, y + h, 8, -4)

  love.graphics.rectangle("fill", x + w, y + h, -4, -8)
  love.graphics.rectangle("fill", x + w, y + h, -8, -4)

  love.graphics.setColor(255,255,255)

  love.graphics.rectangle("fill", x + 1, y + 1, 2, 6)
  love.graphics.rectangle("fill", x + 1, y + 1, 6, 2)

  love.graphics.rectangle("fill", x + w - 1, y + 1, -2, 6)
  love.graphics.rectangle("fill", x + w - 1, y + 1, -6, 2)

  love.graphics.rectangle("fill", x + 1, y + h - 1, 2, -6)
  love.graphics.rectangle("fill", x + 1, y + h - 1, 6, -2)

  love.graphics.rectangle("fill", x + w - 1, y + h - 1, -2, -6)
  love.graphics.rectangle("fill", x + w - 1, y + h - 1, -6, -2)

end

return menuUtils
