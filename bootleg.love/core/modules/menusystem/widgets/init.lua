-- widgets :: basic widget object

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Widget = {}

BaseWidget  = Object:extend()
TextWidget  = BaseWidget:extend()

-- INIT FUNCTIONS
-- Initialize and configure the widget

function BaseWidget:new(menu)
  self.menu = menu

  self.destroyed  = false
  self.selectable = false
  self.selection_margin = 0
  self.margin = 2

  self.canvas = {}
  self.canvas.texture = nil
  self.canvas.needRedraw = true

  self.order = 0
  self:register()
end

function BaseWidget:register()
  self.creationID = self.menu:getWidgetNumber()
  self.menu:addWidget(self)
end

function BaseWidget:redrawCanvas()
  self.width, self.height = self.menu:getWidgetSize(self.id)

  local canvas = love.graphics.newCanvas(self.width, self.height)
  love.graphics.setCanvas( canvas )

  self:drawCanvas()
  self.canvas.needRedraw = false

  love.graphics.setCanvas( )
  local imageData = canvas:newImageData( )
  self.canvas.texture = love.graphics.newImage( imageData )
  canvas:release( )
  imageData:release( )
end

function BaseWidget:invalidateCanvas()
  self.canvas.needRedraw = true
end

function BaseWidget:drawCanvas()
  self.r = love.math.random(128)/256
  self.g = love.math.random(128)/256
  self.b = love.math.random(128)/256

  love.graphics.setColor(self.r, self.g, self.b, 70)
  love.graphics.rectangle("fill", 0, 0, self.width, self.height)
  love.graphics.setColor(self.r, self.g, self.b)
  love.graphics.rectangle("line", 0, 0, self.width, self.height)
  utils.graphics.resetColor()
end

function BaseWidget:selectAction()
  -- Do nothing
end

-- DRAW WIDGETS
-- Draw the widget

function BaseWidget:draw(x, y)
  if self.canvas.texture ~= nil then
    utils.graphics.resetColor()
    love.graphics.draw(self.canvas.texture, x, y)
  end
end

function BaseWidget:drawSelected(x,y,w,h)
    self:draw(x, y, w, h)
end

-- UPDATE FUNCTIONS
-- Update the widget

function BaseWidget:update(dt)
  if (self.canvas.needRedraw) then
    self:redrawCanvas()
  end
 -- N/A
end

-- ACTION FUNCTION
-- Functions to handle actions and selection.

function BaseWidget:action(source)
  --self:destroy()
end

function BaseWidget:destroy()
  self.destroyed = true
end

-- TEXT WIDGET
-- Simple text widget

function TextWidget:new(menu, font, label)
  TextWidget.super.new(self, menu)
  self.font = font
  self.label = label
end

function TextWidget:drawCanvas()
  local w, h
  w = math.floor(self.width / 2)
  h = math.floor(self.height / 2) - (self.font:getHeight() / 2)
  self.font:draw(self.label, w, h, -1, "center")
end

-- Add the widget as subvariable to the returned table
Widget.Base   = BaseWidget
Widget.Text   = TextWidget

return Widget
