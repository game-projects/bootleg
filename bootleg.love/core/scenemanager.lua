-- scene.lua :: a basic scene management system, that work by sending the different
-- core functions to the scene, normally without the scene itself having to manage
-- them.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local SceneManager  = Object:extend()

-- INIT FUNCTIONS
-- Initialize and configure the scene manager

function SceneManager:new(controller)
  self.controller   = controller
  self.timers       = self.controller.modules.Timers(self)
  self.currentScene = nil

  self.storage      = {}

  self:initTransitions()
end

function SceneManager:setScene(scene)
  if self.transition.isPrepared then
    self:startTransition(scene)
  else
    self.currentScene = scene
    self.currentScene.isActive = true
  end
end

function SceneManager:storeCurrentScene(name)
  self.storage[name] = self.currentScene
end

function SceneManager:setStoredScene(name)
  local storedScene = self.storage[name]
  if storedScene ~= nil then
    self.currentScene   = storedScene
    self.storage[name]  = nil
  end
end

function SceneManager:clearStorage()
  self.storage = {}
end

function SceneManager:clearScene()
  self.currentScene = nil
end

-- UPDATE FUNCTIONS
-- Update the current scene and its subobjects

function SceneManager:update(dt)
  self.timers:update(dt)
  if (self.currentScene ~= nil) then
    self.currentScene:updateStart(dt)
    self.currentScene:setKeys()
    self.currentScene.assets:update(dt)
    self.currentScene.menusystem:update(dt)
    self.currentScene:updateWorld(dt)
    self.currentScene:update(dt)
    self.currentScene:updateEnd(dt)
  end
end

-- MOUSE FUNCTIONS
-- Send pointer data to the scene

function SceneManager:mousemoved(x, y, dx, dy)
  if (self.currentScene ~= nil) then
    self.currentScene.mouse.x,
    self.currentScene.mouse.y = x, y
    self.currentScene:mousemoved(x, y, dx, dy)
    self.currentScene.menusystem:mousemoved(x, y, dx, dy)
  end
end

function SceneManager:mousepressed( x, y, button, istouch )
  if (self.currentScene ~= nil) then
    self.currentScene:mousepressed( x, y, button, istouch )
    self.currentScene.menusystem:mousepressed( x, y, button, istouch )
  end
end

-- KEYBOARD FUNCTIONS
-- Add send keys functions to the scene

function SceneManager:keypressed( key, scancode, isrepeat )
  self.currentScene:keypressed( key, scancode, isrepeat )
end

function SceneManager:keyreleased( key )
  self.currentScene:keyreleased( key )
end

-- TRANSITION FUNCTIONS
-- Prepare transitionning to the next scene

function SceneManager:initTransitions()
  self.transition = {}

  self.transition.easeIn      = "inQuad"
  self.transition.easeOut     = "outQuad"
  self.transition.duration    = 1

  self.transition.nextScene   = nil
  self.transition.isPrepared  = false
end

function SceneManager:prepareTransition(duration, easeIn, easeOut)
  self.transition.easeIn      = easeIn    or self.transition.easeIn
  self.transition.easeOut     = easeOut   or self.transition.easeOut
  self.transition.duration    = duration  or self.transition.duration

  self.transition.isPrepared  = true
end

function SceneManager:startTransition(nextScene)
  self.transition.nextScene = nextScene
  self.currentScene:flushKeys(self.transition.duration)
  self.currentScene.isActive = false
  self.transition.nextScene.isActive = false
  core.screen:fadeIn(self.transition.duration / 2.5, self.transition.easeIn)
  self.timers:newTimer(self.transition.duration / 2, "fadeOut")
end

function SceneManager:timerResponse(timer)
  if timer == "fadeOut" then
    self.currentScene = self.transition.nextScene
    self.currentScene:flushKeys(self.transition.duration / 2.5)
    self.currentScene.isActive = false
    core.screen:fadeOut(self.transition.duration / 2.5, self.transition.easeOut)
    self.transition.isPrepared = false
    self.timers:newTimer(self.transition.duration / 2.5, "activateScene")
  elseif timer == 'activateScene' then
    self.currentScene.isActive = true
  end
end

-- DRAW FUNCTIONS
-- Draw the current scene

function SceneManager:draw()
  self.controller.screen:apply()
  if (self.currentScene ~= nil) then
    self.currentScene:drawStart()
    self.currentScene:drawWorld()
    self.currentScene:draw()
    self.currentScene.menusystem:draw()
    self.currentScene:drawEnd()
  end
  self.controller.screen:cease()
end

return SceneManager
