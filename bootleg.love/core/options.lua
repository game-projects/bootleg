-- core/options.lua :: The options loading/saving system. Is used by the other
-- modules to save their settings.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local OptionsManager = Object:extend()

local cwd  = (...):gsub('%.options$', '') .. "."
local binser = require(cwd .. "modules.gamesystem.libs.binser")

local TRANSLATION_PATH = "datas/languages/"

-- INIT FUNCTIONS
-- Initialize and configure the game options

function OptionsManager:new(controller)
  self.controller = controller
  -- We begin by creating an empty data table before reading the data.
  self.data = {}
  self:read()
end

function OptionsManager:reset()
  -- Reset the option to the game defaults.
  self.data.video = {}
  self.data.video.crtfilter   = false
  self.data.video.resolution  = 1
  self.data.video.border      = true
  self.data.video.vsync       = true
  self.data.video.fullscreen  = false

  -- We load the default files
  self.data.input     = self:getInputDefaultData()

  -- TODO: have a way to auto-load a language according to the OS ?
  self.data.language  = self:getTranslationDefaultData()

  self.data.audio = {}
  self.data.audio.music = 100
  self.data.audio.sfx   = 100
end

-- INFO FUNCTIONS
-- Get informations from the option managers

function OptionsManager:getFile(absolute)
  local dir = ""
  if absolute then
    dir = love.filesystem.getSaveDirectory() .. "/"
    if not utils.filesystem.exists(dir) then
      love.filesystem.createDirectory( "" )
    end
  end

  local filepath = dir .. "options.data"

  return filepath
end

function OptionsManager:getInputDefaultData()
  local _path = "datas/inputs.lua"
  local datas = {}
  local fileinfo = love.filesystem.getInfo(_path)

  if fileinfo ~= nil then
    datas = require "datas.inputs"
  else
    datas = {}
  end

  return datas
end

function OptionsManager:getPlayerInputData(id)
  local _playerInputData = self.data.input[id]

  if _playerInputData == nil then
    _playerInputData      = {}
    _playerInputData.keys = {}
  end

  return _playerInputData
end

function OptionsManager:getInputData()
  return self.data.input
end

function OptionsManager:setInputKey(sourceid, padkey, key)
  if self.data.input[sourceid] ~= nil then
    if self.data.input[sourceid].keys[padkey] ~= nil then
      self.data.input[sourceid].keys[padkey] = key
    end
  end
end

-- Lang data

function OptionsManager:getTranslationDefaultData()
  local _path = TRANSLATION_PATH .. "init.lua"
  local fileinfo = love.filesystem.getInfo(_path)
  local datas = nil

  if fileinfo ~= nil then
    lang = require(TRANSLATION_PATH)
    lang.current  = lang.default or "en"
    lang.path     = TRANSLATION_PATH
  end

  return lang
end

function OptionsManager:setLanguage(lang)
  if (self.controller.lang:isLangAvailable(lang)) then
    self.data.language.current = lang
    self.controller.lang:getTranslationData()
  end
end

-- DATA HANDLING FUNCTIONS
-- Save and get data from the savefile

-- FIXME: maybe subclass a special module for that ?

function OptionsManager:write()
  local data = self:getData()

  filepath = self:getFile(true)
  binser.writeFile(filepath, data)
end

function OptionsManager:read()
  filepath = self:getFile(true)
  if utils.filesystem.exists("options.data") then
    local loadedDatas = binser.readFile(filepath)
    self.controller.debug:print("core/options", "data file found, loading it")
    self:setData(loadedDatas[1])
  else
    self:reset()
    self.controller.debug:print("core/options", "no data file found, reseting data")
  end
end

function OptionsManager:getData(data)
  return self.data
end

function OptionsManager:setData(data)
  self.data = data
end

return OptionsManager
