-- core/screen.lua :: Basic screen manager. Use CScreen as a backend, and works
-- as an abstraction layer around CScreen.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local ScreenManager = Object:extend()

local cwd  = (...):gsub('%.screen$', '') .. "."
local CScreen = require(cwd .. "libs.cscreen")

-- INIT FUNCTIONS
-- Initialize and configure the screen manager

function ScreenManager:new(controller)
  self.controller = controller
  self.data       = self.controller.options.data.video
  self.width, self.height = love.graphics.getDimensions()
  self:applySettings()
  CScreen.init(self.width, self.height, true)
  CScreen.setColor(0, 0, 0, 1)

  love.graphics.setDefaultFilter( "nearest", "nearest", 1 )

  self.timers = self.controller.modules.Timers(self)

  self.transitionValue = 0
end

function ScreenManager:applySettings()
  self.data       = self.controller.options.data.video

  local flags = {}
  flags.vsync = self.data.vsync
  flags.borderless = (self.data.border == false)

  love.window.setMode(self.width * self.data.resolution, self.height * self.data.resolution, flags)
  love.window.setFullscreen( self.data.fullscreen )

  local width, height = love.window.getMode()
  CScreen.update(width, height)
end

-- POINTER FUNCTIONS
-- Translate the pointer according to the screen coordinates

function ScreenManager:project(x, y)
  return CScreen.project(x, y)
end

function ScreenManager:getMousePosition()
  return CScreen.project(love.mouse.getX(), love.mouse.getY())
end

function ScreenManager:getScale()
  return CScreen.getScale()
end

function ScreenManager:getScreenCoordinate(x, y)
  return CScreen.getScreenCoordinate(x, y)
end

-- INFO FUNCTIONS
-- Get screen informations

function ScreenManager:getDimensions()
  return self.width, self.height
end

-- SCISSOR FUNCTIONS
-- Simple scissor functions

function ScreenManager:setScissor(x, y, width, height)
  local x, y = self:getScreenCoordinate(x, y)
  love.graphics.setScissor(x, y, width*self.data.resolution, height*self.data.resolution)
end

function ScreenManager:resetScissor()
  love.graphics.setScissor( )
end

-- UPDATE FUNCTIONS
-- Update the screen

function ScreenManager:update(dt)
  self.timers:update(dt)
end

-- TRANSITION FUNCTIONS
-- Handle transitions

function ScreenManager:fadeIn(duration, easing)
  local duration  = duration or 1
  local easing    = easing or "inExpo"
  self.timers:newTween(0, duration, {transitionValue = 1}, easing)
end

function ScreenManager:fadeOut(duration, easing)
  local duration  = duration or 1
  local easing    = easing or "outExpo"
  self.timers:newTween(0, duration, {transitionValue = 0}, easing)
end

function ScreenManager:drawFade()
  local w, h = self:getDimensions()
  love.graphics.setColor(0, 0, 0, self.transitionValue)
  love.graphics.rectangle("fill", 0, 0, w, h)
  utils.graphics.resetColor()
end

-- DRAW FUNCTIONS
-- Apply draw functions to the scene

function ScreenManager:apply()
  CScreen.apply()
end

function ScreenManager:cease()
  CScreen.cease()
end

return ScreenManager
