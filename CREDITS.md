# Credits

## GAMECORE credits

- [Binser](https://github.com/bakpakin/binser) under MIT Licence

- [Classic](https://github.com/rxi/classic) under MIT Licence

- An altered version of [CScreen](https://github.com/CodeNMore/CScreen) under a custom licence.

- [LoveBird](https://github.com/rxi/lovebird) under MIT Licence

- [Bump.lua](https://github.com/kikito/bump.lua), under MIT Licence

- [Bump.3DPD](https://github.com/oniietzschan/bump-3dpd), under MIT Licence

- [Tsort](https://github.com/bungle/lua-resty-tsort), under BSD-2 Clause

- [Simple Tiled Implementation](https://github.com/karai17/Simple-Tiled-Implementation), under MIT Licence

- [tween.lua](https://github.com/kikito/tween.lua), under MIT Licence
